const http = require ('http');
http.createServer((req,resp)=>{
    let userIP = req.connection.remoteAddress;
    resp.writeHead(200,{'Content-Type':'application/json'});
    let out= { 
        requester: userIP,
        info: {name:'Daniel Cerezo', age:'23', profession:'Developer'}
    }
    resp.write(JSON.stringify(out));
    resp.end();
}).listen(8080);
console.log('Listen port 8080');