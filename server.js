const express = require('express'); 
const hbs = require('hbs');
const app = express();
const staticOps = {
    dotfiles:'ignore',
	maxAge: '43200000'
}
app.use(express.static(__dirname+'/statics',staticOps));
app.set('view engine', 'hbs');
app.get('/',(req,resp)=>{
   resp.render('home',{
       name:"Visitor",
       anyo:new Date().getFullYear()
   });//Si la carpeta se llama views no es necesario especificar el path
});
app.listen(8080,()=>{
    
console.log('Listen 8080');
});



